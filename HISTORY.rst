.. :changelog:

History
-------

1.0.3 (2020-05-21)
++++++++++++++++++

* Update dependencies

1.0.2 (2019-06-26)
++++++++++++++++++

* Add dependencies for migration

1.0.1 (2019-05-23)
++++++++++++++++++

* Add domain in SQL view

1.0.0 (2019-01-30)
++++++++++++++++++

* SQL view for kamailio

0.9.0 (2018-12-07)
++++++++++++++++++

* First release on PyPI.
