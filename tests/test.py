#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase
from django.urls import reverse
from django.test import Client

from pyfb_company.models import Company, Customer, Provider, CompanyBalanceHistory

from .models import Did, RoutesDid


def create_company(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["address"] = "address"
    defaults["contact_name"] = "contact_name"
    defaults["contact_phone"] = "contact_phone"
    defaults["customer_balance"] = '1.5'
    defaults["supplier_balance"] = '1.5'
    defaults.update(**kwargs)
    return Company.objects.create(**defaults)


def create_customer(**kwargs):
    defaults = {}
    defaults["account_number"] = "account_number"
    defaults["credit_limit"] = '1.5'
    defaults["low_credit_alert"] = '1.5'
    defaults["max_calls"] = 1
    defaults["calls_per_second"] = 1
    defaults["customer_enabled"] = 1
    defaults.update(**kwargs)
    if "company" not in defaults:
        defaults["company"] = create_company()
    return Customer.objects.create(**defaults)


def create_provider(**kwargs):
    defaults = {}
    defaults["supplier_enabled"] = 1
    defaults.update(**kwargs)
    if "company" not in defaults:
        defaults["company"] = create_company()
    return Provider.objects.create(**defaults)


def create_did(**kwargs):
    defaults = {}
    defaults["number"] = "number"
    defaults["prov_max_channels"] = 10
    defaults["cust_max_channels"] = 10
    defaults["insee_code"] = "insee_code"
    defaults["description"] = "description"
    defaults["provider_free"] = 1
    defaults["customer_free"] = 1
    defaults.update(**kwargs)
    if "provider" not in defaults:
        defaults["provider"] = create_'provider'()
    if "customer" not in defaults:
        defaults["customer"] = create_'customer'()
    return Did.objects.create(**defaults)


def create_routesdid(**kwargs):
    defaults = {}
    defaults["order"] = 1
    defaults["type"] = "s"
    defaults["number"] = "number"
    defaults["description"] = "description"
    defaults["weight"] = 1
    defaults.update(**kwargs)
    if "contract_did" not in defaults:
        defaults["contract_did"] = create_'did'()
    if "trunk" not in defaults:
        defaults["trunk"] = create_'customerdirectory'()
    return RoutesDid.objects.create(**defaults)


class DidViewTest(TestCase):
    '''
    Tests for Did
    '''
    def setUp(self):
        self.client = Client()

    def test_list_did(self):
        url = reverse('pyfb-did:pyfb_did_did_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_did(self):
        url = reverse('pyfb-did:pyfb_did_did_create')
        data = {
            "number": "number",
            "prov_max_channels": 10,
            "cust_max_channels": 10,
            "insee_code": "insee_code",
            "description": "description",
            "provider_free": 1,
            "customer_free": 1,
            "provider": create_'provider'().pk,
            "customer": create_'customer'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_did(self):
        did = create_did()
        url = reverse('pyfb-did:pyfb_did_did_detail', args=[did.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_did(self):
        did = create_did()
        data = {
            "number": "number",
            "prov_max_channels": 10,
            "cust_max_channels": 10,
            "insee_code": "insee_code",
            "description": "description",
            "provider_free": 1,
            "customer_free": 1,
            "provider": 1,
            "customer": 1,
        }
        url = reverse('pyfb-did:pyfb_did_did_update', args=[did.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class RoutesDidViewTest(unittest.TestCase):
    '''
    Tests for RoutesDid
    '''
    def setUp(self):
        self.client = Client()

    def test_list_routesdid(self):
        url = reverse('pyfb-did:pyfb_did_routesdid_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_routesdid(self):
        url = reverse('pyfb-did:pyfb_did_routesdid_create')
        data = {
            "order": 1,
            "type": "s",
            "number": "number",
            "description": "description",
            "weight": 1,
            "contract_did": create_'did'().pk,
            "trunk": create_'customerdirectory'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_routesdid(self):
        routesdid = create_routesdid()
        url = reverse('pyfb-did:pyfb_did_routesdid_detail', args=[routesdid.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_routesdid(self):
        routesdid = create_routesdid()
        data = {
            "order": 1,
            "type": "s",
            "number": "number",
            "description": "description",
            "weight": 1,
            "contract_did": 1,
            "trunk": 1,
        }
        url = reverse('pyfb-did:pyfb_did_routesdid_update', args=[routesdid.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
