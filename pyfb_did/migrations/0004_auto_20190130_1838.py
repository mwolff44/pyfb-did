# Generated by Django 2.1.5 on 2019-01-30 18:38

from django.db import migrations
import migrate_sql.operations


class Migration(migrations.Migration):

    dependencies = [
        ('pyfb_did', '0003_auto_20181221_1627'),
    ]

    operations = [
        migrate_sql.operations.CreateSQL(
            name='dbaliases_view',
            sql="DROP VIEW IF EXISTS dbaliases CASCADE; CREATE OR REPLACE VIEW dbaliases AS  SELECT row_number() OVER () AS id,  d.number AS alias_username,  '' AS alias_domain,  cd.name AS username,  '' AS domain FROM pyfb_did_routes dr LEFT JOIN pyfb_did d  ON dr.contract_did_id = d.id AND dr.type = 's' LEFT JOIN pyfb_endpoint_customer cd  ON dr.trunk_id = cd.id ",
            reverse_sql='DROP dbaliases',
        ),
    ]
