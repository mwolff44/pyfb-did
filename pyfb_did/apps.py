# -*- coding: utf-8
from django.apps import AppConfig


class PyfbDidConfig(AppConfig):
    name = 'pyfb_did'
