=====
Usage
=====

To use pyfb-did in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'pyfb_did.apps.PyfbDidConfig',
        ...
    )

Add pyfb-did's URL patterns:

.. code-block:: python

    from pyfb_did import urls as pyfb_did_urls


    urlpatterns = [
        ...
        url(r'^', include(pyfb_did_urls)),
        ...
    ]
