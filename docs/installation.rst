============
Installation
============

At the command line::

    $ easy_install pyfb-did

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv pyfb-did
    $ pip install pyfb-did
